#ifndef INTEGRATE_TEAM_H
#define INTEGRATE_TEAM_H

// Librairies
#include <iostream>
#include <string>

/**
 * \file integrate_team.h
 * \brief definition of integrate_team class
 * \version 0.0.1
 * \author LECOMTE Victor  
 */
class Integrate_Team
{

private :
  int individual_time = 0; ///< individual_time as int, initialized at 0  
  std::string bib_number; ///< bib_number as sring
    
public :
// Default constructor
  /**
   * \brief Default constructor
   */
   Integrate_Team();

// Constructor with parameters
  /**
   * \brief Constructors with differents parameters
   * \param individual_time  - the individual_time of a team
   * \param bib_number - the bib_number for a rider of the team
   */
   Integrate_Team(int individual_time, std::string bib_number);

// Destructive
  /**
   * \brief Default destructor of an integrate_team 
   */
   ~Integrate_Team();

// Accessors (getters)
  /**
   * \brief Allows the recovery of the individual_time
   * \return int representing the individual_time of a team
   */
   int getIndividual_Time();

  /**
   * \brief Allows the recovery of the bib_number 
   * \return string representing the bib_number of a team
   */
   std::string getBib_Number();

// Mutators (setters)
  /**                                                           
   * \brief Allows the modification of the individual_time 
   */
   void setIndividual_Time(int);

  /**        
   * \brief Allows the modification of the bib_number
   */
   void setBib_Number(std::string);

// Methods
  /**
   *  Allows the display of all parameters (attributes)
   */
  std::string toString();
};
#endif // INTEGRATE_TEAM_H
