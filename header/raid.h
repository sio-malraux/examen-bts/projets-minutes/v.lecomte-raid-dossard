#ifndef RAID_H
#define RAID_H

// Librairies
#include <iostream>
#include <string>

/**
 * \file raid.h
 * \brief definition of raid class
 * \version 0.0.1
 * \author LECOMTE Victor  
 */
class Raid 
{

private :
  std::string code; ///< code as string
  std::string name; ///< name as string
  std::string start_date; ///< start_date as string
  std::string town; ///< town as string
  std::string region; ///< region as string
  int nb_max_per_team; ///< nb_max_per_team as int 
  float registration_amount; ///< resgistration_amount as float
  int nb_women; ///< nb_women as int 
  int maxi_time; ///< maxi_time as int 
  int age_minimum; ///< age_minimum as int 


public :
// Default constructor
  /**
   * \brief Default constructor
   */
   Raid();

// Constructor with parameters
  /**
   * \brief Constructors with differents parameters
   * \param code -  the code of a raid 
   * \param name -  the name of a raid
   * \param start_date -  the start_date of a raid
   * \param town -  the town of a raid
   * \param region -  the region of a raid
   * \param nb_max_per_team - The maximum number of people that can register in a team for a raid is
   * \param registration_amount -  the registration_amount of a raid
   * \param nb_women -  the nb_women of a raid 
   * \param maxi_time -  the maxi_time of a raid
   * \param age_minimum -  the age_minimum of a raid 
   */
   Raid(std::string code, std::string name, std::string start_date, std::string town, std::string region, int nb_max_per_team, float registration_amount, int nb_women, int maxi_time, int age_minimum);

// Destructive
  /**
   * \brief Default destructor of a raid 
   */
   ~Raid();

// Accessors (getters)
  /**
   * \brief Allows the recovery of the code 
   * \return string representing the code of a raid
   */
   std::string getCode();

  /**
   * \brief Allows the recovery of the name 
   * \return string representing the name of a raid
   */
   std::string getName();

  /**   
   * \brief Allows the recovery of the start_date   
   * \return string representing the start_date of a raid
   */
   std::string getStart_Date();

  /**   
   * \brief Allows the recovery of the town 
   * \return string representing the town of a raid                                                                             
   */
   std::string getTown();

  /**
   * \brief Allows the recovery of the region
   * \return string representing the region of a raid
   */
   std::string getRegion();

  /**
   * \brief Allows the recovery of the nb_max_per_team
   * \return int representing the nb_max_per_team of a raid
   */
   int getNb_Max_Per_Team();

  /**                                                                                                                      
   * \brief Allows the recovery of the registration_amount 
   * \return float representing the registration_amount of a raid  
   */
   float getRegistration_Amount();

  /** 
   * \brief Allows the recovery of the nb_women
   * \return int representing the nb_women of a raid
   */
   int getNb_Women();

  /**  
   * \brief Allows the recovery of the maxi_time
   * \return int representing the maxi_time of a raid
   */
   int getMaxi_Time();

  /**
   * \brief Allows the recovery of the age_minimum
   * \return int representing the age_minimum of a raid  
   */
   int getAge_Minimum();
    
// Mutators (setters)
  /**      
   * \brief Allows the modification of the code     
   */
   void setCode(std::string);

  /** 
   * \brief Allows the modification of the name         
   */
   void setName(std::string);

  /**    
   * \brief Allows the modification of the start_date         
   */
   void setStart_Date(std::string);

  /**   
   * \brief Allows the modification of the town    
   */
   void setTown(std::string);

  /**       
   * \brief Allows the modification of the region        
   */
   void setRegion(std::string);

  /**                    
   * \brief Allows the modification of the nb_max_per_team   
   */
   void setNb_Max_Per_Team(int);

  /**         
   * \brief Allows the modification of the registration_amount
   */
   void setRegistration_Amount(float);

  /**      
   * \brief Allows the modification of the nb_women          
   */
   void setNb_Women(int);

  /**    
   * \brief Allows the modification of the maxi_time         
   */
   void setMaxi_Time(int);

  /**                      
   * \brief Allows the modification of the age_minimum
   */
   void setAge_Minimum(int);

// Methods
  /**
   *  Allows the display of all parameters (attributes)
   */
  std::string toString();
};
#endif //  RAID_H
