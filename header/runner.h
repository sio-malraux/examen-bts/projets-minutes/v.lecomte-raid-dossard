#ifndef RUNNER_H
#define RUNNER_H

// Librairies
#include <iostream>
#include <string>

/**
 * \file runner.h
 * \brief definition of runner class
 * \version 0.0.1
 * \author LECOMTE Victor  
 */
class Runner
{

private :
  std::string license; ///< license as string
  std::string name; ///< name as string
  std::string first_name; ///< first_nale as string
  std::string sex; ///< sex as string 
  std::string nationality; ///< nationality as string
  std::string mail; ///< mail as string 
  bool certif_med_aptitude = false; ///< certif_med_aptitude as bool, initialized at false 
  std::string date_of_birth; ///< date_of_birth as string
    

public : 
// Default constructor
  /**
   * \brief Default constructor
   */
   Runner();

// Constructor with parameters
  /**
   * \brief Constructors with differents parameters
   * \param license - the license of a runner 
   * \param name - the name of a runner
   * \param first_name - the first_name of a runner
   * \param sex - the sex of a runner
   * \param nationality - the nationality of a runner
   * \param mail - the mail of a runner
   * \param certif_med_aptitude - the certif_med_aptitude as a runner
   * \param date_of_birth -  the date_of_birth of a runner
   */
   Runner(std::string license, std::string name, std::string first_name, std::string sex, std::string nationality, std::string mail, bool certif_med_aptitude, std::string date_of_birth);

// Destructive
  /**
   * \brief Default destructor of an runner 
   */
  ~Runner();

// Accessors (getters)
  /**
   * \brief Allows the recovery of the license 
   * \return string representing the license of a runner
   */
   std::string getLicense();

  /**          
   * \brief Allows the recovery of the name 
   * \return string representing the name of a runner  
   */
   std::string getName();

  /**
   * \brief Allows the recovery of the first_name  
   * \return string representing the first_name of a runner 
   */
   std::string getFirst_Name();
  
  /**
   * \brief Allows the recovery of the sex     
   * \return string representing the sex of a runner   
   */
   std::string getSex();

  /**
   * \brief Allows the recovery of the nationality 
   * \return string representing the nationality of a runner
   */
   std::string getNationality();

  /**
   * \brief Allows the recovery of the mail 
   * \return string representing the mail of a runner
   */
   std::string getMail();

  /**
   * \brief Allows the recovery of the certif_med_aptitude 
   * \return bool representing the certif_med_aptitude of a runner
   */
   bool getCertif_Med_Aptitude();

  /**
   * \brief Allows the recovery of the date_of_birth 
   * \return string representing the date_of_birth of a runner
   */
   std::string getDate_Of_Birth();
   
// Mutators (setters)
  /**      
   * \brief Allows the modification of the license          
   */
   void setLicense(std::string);

  /**          
   * \brief Allows the modification of the name     
   */
   void setName(std::string);

  /** 
   * \brief Allows the modification of the first_name 
   */
   void setFirst_Name(std::string);

  /**                  
   * \brief Allows the modification of the sex  
   */
   void setSex(std::string);

  /**     
   * \brief Allows the modification of the nationality
   */
   void setNationality(std::string);

  /**                           
   * \brief Allows the modification of the mail 
   */
   void setMail(std::string);

  /**        
   * \brief Allows the modification of the certif_med_aptitude
   */
   void setCertif_Med_Aptitude(bool);

  /**         
   * \brief Allows the modification of the date_of_birth
   */
   void setDate_Of_Birth(std::string);

// Methods
  /**
   *  Allows the display of all parameters (attributes)
   */
  std::string toString();
};
#endif // RUNNER_H
