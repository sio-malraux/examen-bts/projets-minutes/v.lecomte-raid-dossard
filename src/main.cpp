#include <iostream>
#include <string>
#include <libpq-fe.h>
#include <tclap/CmdLine.h>
#include <syslog.h>
#include "../header/connection.h"



int main(int argc, char *argv[])
{
  try
  {
    TCLAP::CmdLine cmd("The main goal is to generate a bib number, and verify the teams, numbers of women in team", ' ', "0.1");
    TCLAP::ValueArg<std::string> raidArg("r", "raid", "The raid code the user is going to enter", true, "A02BA", "string");
    cmd.add(raidArg);
    cmd.parse(argc, argv);
    std::string raid = raidArg.getValue();
    Connection connection;
    connection.user("v.lecomte");
    std::string rqt_exist = "SELECT * FROM raid_dossard.raid;";
    // std::string rqt_exist = "SELECT raid_dossard.verif_raid_exist('" + raid + "');";
    PGresult *resultat = connection.exec(rqt_exist);

    if(resultat != nullptr)
    {
      if(std::string(PQgetvalue(resultat, 0, 0)) == "f")
      {
        std::cout << "The raid " << raid << " doesn't exist" << std::endl;
      }
      else if(std::string(PQgetvalue(resultat, 0, 0)) == "t")
      {
        std::cout << "The raid " << raid << " exist" << std::endl;
      }
    }
  }
  catch(TCLAP::ArgException &e)     // catch any exceptions
  {
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
  }
  return 0;
}
