// Librairies
#include <iostream>
#include <string>
#include "../header/runner.h"

// Default constructor
Runner::Runner() :
  license("VT990"),
  name("LECOMTE"),
  first_name("Victor"),
  sex("H"),
  nationality("Français"),
  mail("victor.lecomte@bts-malraux.net"),
  certif_med_aptitude(),
  date_of_birth("27/05/1999")
{
  std::cout << "The default constructor is composed as follows" << std::endl;
  std::cout << "The license is : " << license << std::endl;
  std::cout << "The name is : " << name << std::endl;
  std::cout << "The first name is : " << first_name << std::endl;
  std::cout << "The sex is : " << sex << std::endl;
  std::cout << "The nationality is : " << nationality << std::endl;
  std::cout << "The certificate is : " << certif_med_aptitude << std::endl;
  std::cout << "The date of birth is : " << date_of_birth << std::endl;
}
 

// Constructor with parameters
Runner::Runner(std::string _license, std::string _name, std::string _first_name, std::string _sex, std::string _nationality, std::string _mail, bool _certif_med_aptitude, std::string _date_of_birth) : 
  license(_license),
  name(_name),
  first_name(_first_name),
  sex(_sex),
  nationality(_nationality),
  mail(_mail),
  certif_med_aptitude(_certif_med_aptitude),
  date_of_birth(_date_of_birth)
{
  std::cout << "The constructor with parameter is composed as follows "  << std::endl;
  std::cout << "The license is : " << license << std::endl;
  std::cout << "The name is : " << name << std::endl;
  std::cout << "The first name is : " << first_name << std::endl;
  std::cout << "The sex is : " << sex << std::endl;
  std::cout << "The nationality is : " << nationality << std::endl;
  std::cout << "The certificate is : " << certif_med_aptitude << std::endl;
  std::cout << "The date of birth is : " << date_of_birth << std::endl;
}

// Destructive 
Runner::~Runner() {}

// Accessors (getters)                                                                  
std::string Runner::getLicense()
{
  return license; 
}

std::string Runner::getName()
{
 return name;
}

std::string Runner::getFirst_Name()
{
 return first_name;
}
   
std::string Runner::getSex()
{
 return sex;
}
   
std::string Runner::getNationality()
{
 return nationality;
}
   
std::string Runner::getMail()
{
 return mail;
}

bool Runner::getCertif_Med_Aptitude()
{
 return certif_med_aptitude;
}

std::string Runner::getDate_Of_Birth()
{
 return date_of_birth;
}

// Mutatore (setters)
void Runner::setLicense(std::string _license)
{
 license = _license;
}

void Runner::setName(std::string _name)
{
 name = _name;
}

void Runner::setFirst_Name(std::string _first_name)
{
 first_name = _first_name;
}

void Runner::setSex(std::string _sex)
{
 sex = _sex;
}
   
void Runner::setNationality(std::string _nationality)
{
 nationality = _nationality;
}
   
void Runner::setMail(std::string _mail)
{
 mail = _mail;

}
  
void Runner::setCertif_Med_Aptitude(bool _certif_med_aptitude)
{
 certif_med_aptitude = _certif_med_aptitude;
}
   
void Runner::setDate_Of_Birth(std::string _date_of_birth)
{
 date_of_birth = _date_of_birth;
}


