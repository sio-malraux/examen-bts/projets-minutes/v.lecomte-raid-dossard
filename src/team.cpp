// Librairies
#include <iostream>
#include <string>
#include <chrono>
#include <cstdint>
#include "../header/team.h"

// Default constructor
Team::Team() :
  number(12345),
  raid_code("2020TF"),
  name("Team Sky"),
  record_date(),
  ranking(2),
  global_time(120),
  penalites_bonif(12)
{
  std::cout << "The default constructor is composed as follows" << std::endl;
  std::cout << "The number is : " << number << std::endl;
  std::cout << "The raid code is : " << raid_code << std::endl;
  std::cout << "The name is : " << name << std::endl;
  std::cout << "The record date is : " << record_date << std::endl;
  std::cout << "The ranking is : " << ranking << std::endl;
  std::cout << "The global time is : " << global_time << std::endl;
  std::cout << "The penalites bonif is : " << penalites_bonif << std::endl;
}

Team::Team(int _number, std::string _raid_code, std::string _name, std::chrono::system_clock _record_date, int _ranking,int _global_time, int _penalites_bonif) :
  number(_number),
  raid_code(_raid_code),
  name(_name),
  record_date(_record_date),
  ranking(_ranking),
  global_time(_global_time),
  penalites_bonif(_penalites_bonif)           
{
  std::cout << "The default constructor is composed as follows" << std::endl;
  std::cout << "The number is : " << number << std::endl;
  std::cout << "The raid code is : " << raid_code << std::endl;
  std::cout << "The name is : " << name << std::endl;
  std::cout << "The record date is : " << record_date << std::endl;
  std::cout << "The ranking is : " << ranking << std::endl;
  std::cout << "The global time is : " << global_time << std::endl;
  std::cout << "The penalites bonif is : " << penalites_bonif << std::endl;
}

// Destructive
Team::~Team() {}

// Accessors (getters)
int Team::getNumber()
{
  return number;
}

std::string Team::getRaid_Code()
{
  return raid_code;
}

std::string Team::getName()
{
  return name;
}

std::chrono::system_clock Team::getRecord_Date()
{
  return record_date;
}

int Team::getGlobal_Time()
{
  return global_time;
}

int Team::getRanking()
{
  return ranking;
}

int Team::getPenalites_Bonif()
{
  return penalites_bonif;
}
// Mutators (setters)
void Team::setNumber(int _number)
{
  number = _number;  
}

void Team::setRaid_Code(std::string _raid_code)
{
  raid_code = _raid_code;
}

void Team::setName(std::string _name)
{
  name = _name;
}

void Team::setRecord_Date(std::chrono::system_clock _record_date)
{
  record_date = _record_date;
}

void Team::setRanking(int _ranking)
{
  ranking = _ranking;
}

void Team::setGlobal_Time(int _global_time)
{
  global_time = _global_time;
}

void Team::setPenalites_Bonif(int _penalites_bonif)
{
  penalites_bonif = _penalites_bonif;
}

